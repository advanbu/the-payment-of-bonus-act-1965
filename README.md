# The Name of The Act 0000

Describe the legislature's intent and about the Act etc.

<table>
  <tc>
    <th>Act ID</th>
    <th>Act No</th>
    <th>Act Year</th>
    <th>Enactment Date</th>
    <th>Enforcement Date<sup>1</sup></th>
  </tc>
  <tr>
    <td>000000</td>
    <td>00</td>
    <td>00</td>
    <td>00</td>
    <td>00</td>
  </tr>
</table>

<table>
    <tr>
    <th>Ministry</th>
    <td>00</td>
  </tr>
    <tr>
    <th>Short Title</th>
    <td>00</td>
  </tr>
    <tr>
    <th>Long Title</th>
    <td>00</td>
  </tr>
</table>

1. Vide Notification No.L.3067, dated 24th February 1937, published in the Gazette of India, 1937, Pt I, p.303.
